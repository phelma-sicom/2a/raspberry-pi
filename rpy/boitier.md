<style>
      /* Pour la gestion des iframes */
 .iframe-wrapper {
  margin: auto; /* pour centrer le conteneur */
}

 .container {
  position: relative;
  overflow: hidden;
  width: 100%;
  padding-top: 56.25%; /* 16:9 Aspect Ratio (divide 9 by 16 = 0.5625) */
}
.responsive-iframe {
  position: absolute;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  width: 100%;
  height: 100%;
}
</style>

# 📦 Boîtier

Grâce à l'**impression 3D**, il est possible de créer un **boitier de protection** pour le Raspberry Pi. Voici un exemple :

<div class="iframe-wrapper">
    <div class="container">
        <iframe class="responsive-iframe" width="500" height="150" src="https://www.framboise314.fr/wp-content/uploads/2019/12/boitier_Pi4.mp4" title="Impression 3D"  allowfullscreen></iframe>
    </div>
</div>

Ce tutoriel vous explique **comment le construire** : https://www.framboise314.fr/un-boitier-de-raspberry-pi-4-avec-freecad/