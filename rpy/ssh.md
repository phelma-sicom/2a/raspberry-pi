# 🖥️ Connexion à distance en SSH

Pour **se connecter à distance** à un Raspberry Pi, une des méthodes les plus classiques est d'utiliser une [connexion SSH](https://fr.wikipedia.org/wiki/Secure_Shell), qui opère aux couches 5, 6 et 7 du [modèle OSI](https://fr.wikipedia.org/wiki/Mod%C3%A8le_OSI). Cette connexion repose généralement sur le [protocole TCP](https://fr.wikipedia.org/wiki/Transmission_Control_Protocol) pour la **couche de transport** (couche 4 du [modèle OSI](https://fr.wikipedia.org/wiki/Mod%C3%A8le_OSI)) et le [protocole IP](https://fr.wikipedia.org/wiki/Internet_Protocol) pour la **couche réseau** (couche 3 du [modèle OSI](https://fr.wikipedia.org/wiki/Mod%C3%A8le_OSI)).

Pour les **couches de liaison de données** (couche 2 du [modèle OSI](https://fr.wikipedia.org/wiki/Mod%C3%A8le_OSI)) et physique (couche 1 du [modèle OSI](https://fr.wikipedia.org/wiki/Mod%C3%A8le_OSI)), quatre solutions différentes peuvent être utilisées :

- **Ethernet** connexion [RJ45](https://fr.wikipedia.org/wiki/RJ45) (niveau de complexité : ⭐) : connexion stable et rapide via un câble réseau
- **WiFi** (niveau de complexité : ⭐⭐) : connexion sans fil, pratique pour une utilisation dans des zones sans câblage réseau
- **USB** (niveau de complexité : ⭐⭐⭐) : connexion directe via un câble USB (USB gadget)
- **Bluetooth** (niveau de complexité : ⭐⭐⭐) : connexion directe sans fil plus lente que le Wifi

