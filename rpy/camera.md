<style>
      /* Pour la gestion des iframes */
 .iframe-wrapper {
  max-width: 60%;
  margin: auto; /* pour centrer le conteneur */
}

 .container {
  position: relative;
  overflow: hidden;
  width: 100%;
  padding-top: 56.25%; /* 16:9 Aspect Ratio (divide 9 by 16 = 0.5625) */
}
.responsive-iframe {
  position: absolute;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  width: 100%;
  height: 100%;
}
</style>

# 📸 Utilisation de la caméra

## Connexion de la caméra

<div class="iframe-wrapper">
    <div class="container">
        <iframe class="responsive-iframe" width="300" height="150" src="https://www.youtube-nocookie.com/embed/yhM1NhD-kGs" title="HOW TO Set up the Raspberry Pi Camera Module" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
    </div>
</div>

```{important}
La **version de Raspberry Pi OS** présentée dans la vidéo est **plus ancienne** que celle que l'on utilise. Dans la version de l'OS utilisée (Raspberry Pi OS 64 bits pour Raspberry Pi 4), il n'y **pas besoin d'activer la caméra** dans `raspi-config`, elle est activée par défaut. De même, au lieu d'utiliser `raspistill`, il faut utiliser `libcamera-still`.
```

## Utilisation de la caméra

On va voir **deux manières d'utiliser la caméra** du Raspberry pi : en **lignes de commande** et sous forme de **code Python**.

## En lignes de commande

### Pour des images

Vous pouvez **prendre une photo avec la caméra** en utilisant la commande suivante :

```bash
rpicam-still -o test.jpg
```
Avec :

- `-o` (pour output) est utilisée pour spécifier le **nom de fichier de sortie** pour l'image capturée.

Il peut parfois être intéressant de récupérer des **images de taille inférieure** à la valeur par défaut :

```bash
rpicam-still -o test.jpg --width 64 --height 48
```
Avec :

- `-o` (pour output) utilisée pour spécifier le **nom de fichier de sortie** pour l'image capturée.
- `--width` largeur de l'image en pixels
- `--height` hauteur de l'image en pixels


Pour visualiser l'image, il y a **différentes solutions** suivant comment est connecté le Raspberry Pi :

- **en interface graphique (en local ou à distance par VNC)**

Il suffit d'ouvrir le fichier image créé avec un logiciel de visualisation d'images

- **en SSH par VSCode**

L'arborescence des fichiers du Raspberry Pi est présente à gauche de l'écran. Il suffit de double cliquer sur le fichier d'image créé dans l'arborescence.

- **en SSH**

Il faut récupérer le fichier image sur votre ordinateur local. Vous pouvez en faire en utilisant le <a href="https://fr.wikipedia.org/wiki/Secure_copy" target="_blank">SCP (Secure Copy Protocol)</a>. 

Pour récupérer le fichier image du Raspberry Pi, vous pouvez **adapter avec vos paramètres** la commande suivante à **exécuter sur votre ordinateur local** :

```bash
scp votre_identifiant@adresse_ip_du_raspberry:/chemin/vers/test.jpg /chemin/vers/destination/locale/
```

### Pour de la vidéo

- **Fichier vidéos**

Pour créer une vidéo de 10 secondes, vous pouvez exécuter la commande suivante **sur le Raspberry Pi** :

```bash
rpicam-vid -o video.h264 --framerate 30 --timeout 10000
```
Avec :

- `-o video.h264` spécifie le fichier de sortie pour la vidéo au format <a href="https://fr.wikipedia.org/wiki/H.264" target="_blank">H.264</a>.
- `--framerate 30` définit le taux de rafraîchissement des images de la vidéo.
- `--timeout 10000` définit la durée de l'enregistrement vidéo en millisecondes (ici, 10000 ms = 10 secondes).

La récupération et la visualisation de la vidéo s'effectue de la **même façon que pour l'image**.

- **Flux vidéo**

La commande suivante capture la vidéo de la vidéo en flux vidéo H.264 et utilise la version en ligne de commande du logiciel VLC pour diffuser le flux via HTTP sur le port 8080 **à lancer sur le Raspberry Pi**:

```bash
rpicam-vid -t 0 -o - | cvlc -vvv stream:///dev/stdin --sout '#standard{access=http,mux=ts,dst=:8080}' :demux=h264
```

Pour **voir le flux vidéo** sur un autre ordinateur, il faut **ouvrir le flux vidéo** dans le **logiciel VLC** à l'adresse IP du Raspberry Pi (à modifier avec vos données) : `http://192.168.88.63:8080` 

```{note}
La **latence** peut être **assez forte** en diffusant le flux vidéo de cette façon. La latence est en général **plus faible** en envoyant le flux vidéo depuis un **serveur web Python** comme cela est présenté dans la **suite de cette page**.
```

## En Python

### Pour des images

```python
from picamera2 import Picamera2

# Créer une instance de Picamera2
picam2 = Picamera2()

# Configurer Picamera2 pour ne pas utiliser de prévisualisation car utilisation en ligne de commande
picam2.configure(picam2.create_still_configuration())

# Démarrer la capture sans prévisualisation
picam2.start()

# Capturer une image dans un fichier
picam2.capture_file("test_py.jpg")
```

Problème de DRM si utilisation uniquement de la ligne de commande

### Pour de la vidéo

- **Flux vidéo**

```python
import io
import logging
import socketserver
from http import server
from threading import Condition

from picamera2 import Picamera2
from picamera2.encoders import JpegEncoder
from picamera2.outputs import FileOutput

PAGE = """\
<html>
<head>
<title>picamera2 MJPEG streaming demo</title>
</head>
<body>
<h1>Picamera2 MJPEG Streaming Demo</h1>
<img src="stream.mjpg" width="640" height="480" />
</body>
</html>
"""


class StreamingOutput(io.BufferedIOBase):
    def __init__(self):
        self.frame = None
        self.condition = Condition()

    def write(self, buf):
        with self.condition:
            self.frame = buf
            self.condition.notify_all()


class StreamingHandler(server.BaseHTTPRequestHandler):
    def do_GET(self):
        if self.path == '/':
            self.send_response(301)
            self.send_header('Location', '/index.html')
            self.end_headers()
        elif self.path == '/index.html':
            content = PAGE.encode('utf-8')
            self.send_response(200)
            self.send_header('Content-Type', 'text/html')
            self.send_header('Content-Length', len(content))
            self.end_headers()
            self.wfile.write(content)
        elif self.path == '/stream.mjpg':
            self.send_response(200)
            self.send_header('Age', 0)
            self.send_header('Cache-Control', 'no-cache, private')
            self.send_header('Pragma', 'no-cache')
            self.send_header('Content-Type', 'multipart/x-mixed-replace; boundary=FRAME')
            self.end_headers()
            try:
                while True:
                    with output.condition:
                        output.condition.wait()
                        frame = output.frame
                    self.wfile.write(b'--FRAME\r\n')
                    self.send_header('Content-Type', 'image/jpeg')
                    self.send_header('Content-Length', len(frame))
                    self.end_headers()
                    self.wfile.write(frame)
                    self.wfile.write(b'\r\n')
            except Exception as e:
                logging.warning(
                    'Removed streaming client %s: %s',
                    self.client_address, str(e))
        else:
            self.send_error(404)
            self.end_headers()


class StreamingServer(socketserver.ThreadingMixIn, server.HTTPServer):
    allow_reuse_address = True
    daemon_threads = True


picam2 = Picamera2()
picam2.configure(picam2.create_video_configuration(main={"size": (640, 480)}))
output = StreamingOutput()
picam2.start_recording(JpegEncoder(), FileOutput(output))

try:
    address = ('', 8000)
    server = StreamingServer(address, StreamingHandler)
    server.serve_forever()
finally:
    picam2.stop_recording()
```