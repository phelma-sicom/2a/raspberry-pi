# 🖥️ Connexion à distance avec VS Code

**Visual Studio Code** est un **éditeur de code (IDE)** open source (<a href="https://github.com/microsoft/vscode" target="_blank">code source</a> accessible sur Github) et gratuit. Il est disponible sous **Windows**, **macOS** et **Linux** : <a href="https://code.visualstudio.com/" target="_blank">lien vers le site de téléchargement</a>.

Les **extensions** sont des compléments qui **augmentent les fonctionnalités de base** de l'éditeur VSCode. **Plusieurs extensions** peuvent être utiles pour travailler avec une Raspberry Pi :

- 🐍 <a href="https://marketplace.visualstudio.com/items?itemName=ms-python.python" target="_blank">Python extension</a>

Cette extension **ajoute des fonctionnalités** pour coder en langage **Python** : complétion de code (IntelliSense), débogueur, linting, formatage du code, gestion des environnements virtuels, exécution des notebooks Jupyter...

- 📶 <a href="https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-ssh" target="_blank">Remote - SSH</a>

Cette extension offre une fonctionnalité permettant de **se connecter à un serveur distant via SSH (Secure Shell)** et de travailler sur des projets **comme s'ils étaient locaux**. Elle permet donc de **coder sur le Raspberry Pi** depuis VSCode sur votre ordinateur sans devoir effectuer une connexion SSH et ouvrir un éditeur de code sur le Raspberry Pi.

___

## Mise en place de l'extension Remote - SSH 