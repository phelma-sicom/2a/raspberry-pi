# 💻 Connexion en direct

Pour vous **connecter en direct** sur le Raspberry Pi, il faut les **équipements suivants** :

- une **alimentation** officielle du Raspberry Pi connectée en USB-C
- un **écran** avec une connection HDMI (possibilité de connecter 2 écrans en micro-HDMI)
- un **câble micro-HDMI / HDMI** pour connecter l'écran (ou un câble HDMI/HDMI et un adaptateur micro HDMI/HDMI)
- un **clavier** connecté en USB (port USB2-A de préférence)
- une **souris** connectée en USB (port USB2-A de préférence)
- un disque **SSD** connecté en USB (port USB3-A de préférence) ou une **carte micro-SD** avec un système d'exploitation installé compatible avec le Raspberry Pi

```{important}
Les ports <a href="https://fr.wikipedia.org/wiki/USB_3.0" target="_blank">USB 3.0</a> offrent une **vitesse de transfert nettement supérieure** (jusqu'à 5 Gbps) par rapport aux ports USB 2.0. Ils sont donc à privilégier pour équipements qui demandent une bande passante importante comme par exemple les disques durs externes, les SSD, les clé USB, les caméras USB...

Si vous connectez **deux écrans 4K** au Raspberry Pi, le **taux de rafraîchissement** sera **réduit** (de 60Hz avec un écran à 30Hz avec deux écrans)
```

Schéma de la connexion du Raspberry Pi en direct sur la carte :

<a href="./_static/direct.png" target="_blank"><img src="./_static/direct.png" alt="Connexion en direct" /></a>

``````{note}
Le Raspberry Pi ne possède **pas d'interrupteur "marche/arrêt"**. Il s'allume dès qu'il est connecté au courant. Pour l'**éteindre proprement**, vous devez taper la commande suivante dans le terminal (ou l'éteindre par l'interface graphique du système d'exploitation) :
```bash
sudo halt
```
La DEL verte doit s'éteindre et la DEL rouge reste allumée. Vous pouvez retirer l'alimentation.
``````