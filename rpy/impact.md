# 🌍 Impact environnemental

## Consommation électrique

La **consommation électrique** du Raspberry Pi est **plus faible** que celle des **ordinateurs de bureau** ou des **ordinateurs portables** {cite}`power_consumption`. Néanmoins, cette consommation **tend à augmenter** avec l'amélioration des performances des **derniers modèles**.

Le **processeur** du Raspberry Pi est le composant qui **consomme le plus d'énergie** {cite}`powerpi`. Pour avoir un **ordre de grandeur de sa consommation**, on peut récupérer sa **température** grâce à la commande suivante :

```bash
vcgencmd measure_temp
```

Cette commande peut facilement être intégrée dans un **script bash ou Python** pour collecter de **manière régulière** grâce à une tâche CRON la **température du processeur**.

## Consommation de matériaux et pollutions

Le document suivant présente l'**analyse du cycle de vie** d'un **Raspberry Pi version 1 modèle A+** :

<a href="http://www.designlife-cycle.com/raspberry-pi" target="_blank"><img src="https://images.squarespace-cdn.com/content/v1/5388de33e4b01b17bc0d1ed7/1480558360685-YRORXCOI4V34Y04L0EBN/RP+Poster" alt="Impact Raspberry Pi" /></a>








