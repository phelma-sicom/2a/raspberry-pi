# ⌨️ Prise en main du Raspberry Pi

Le Raspberry est une **carte monocircuit imprimé** de la taille d’une carte de crédit, conçue par des professeurs de l’université de Cambridge dans le cadre d’un projet de promotion de l’informatique. La **première version** du Raspberry Pi est sortie en **2012** et la **dernière version** est le **Raspberry Pi 5** sortie **fin 2023**.

La version du Raspberry Pi utilisée majoritairement **à Phelma** est le **Raspberry Pi 4 modèle B avec 8 Go de RAM**.

Schéma des connecteurs du Raspberry Pi :

<a href="./_static/connecteurs.png" target="_blank"><img src="./_static/connecteurs.png" alt="Connecteurs du raspberry Pi" /></a>

Schéma des composants du Raspberry Pi :

<a href="./_static/composants.png" target="_blank"><img src="./_static/composants.png" alt="Composants du raspberry Pi" /></a>

```{admonition} 📜 Historique des versions du Raspberry Pi

| Révision | Date      | Modèle                                                     | Mémoire        |
|----------|-----------|------------------------------------------------------------|----------------|
| 0002     | Q1 2012   | Model B Revision 1.0                                       | 192 MB         |
| 0003     | Q3 2012   | Model B Revision 1.0 + Fuses mod and D14 removed           | 256 MB         |
| 0004     | Q3 2012   | Model B Revision 2.0 256MB, (Sony)                         | 256 MB         |
| 0005     | Q4 2012   | Model B Revision 2.0 256MB, (Qisda)                        | 256 MB         |
| 0006     | Q4 2012   | Model B Revision 2.0 256MB, (Egoman)                       | 256 MB         |
| 0007     | Q1 2013   | Model A Revision 2.0 256MB, (Egoman)                       | 256 MB         |
| 0008     | Q1 2013   | Model A Revision 2.0 256MB, (Sony)                         | 256 MB         |
| 0009     | Q1 2013   | Model A Revision 2.0 256MB, (Qisda)                        | 256 MB         |
| 000D     | Q4 2012   | Model B Revision 2.0 512MB, (Egoman)                       | 512 MB         |
| 000E     | Q4 2012   | Model B Revision 2.0 512MB, (Sony)                         | 512 MB         |
| 000F     | Q4 2012   | Model B Revision 2.0 512MB, (Qisda)                        | 512 MB         |
| 0010     | Q3 2014   | Model B+ Revision 1.0 512MB, (Sony)                        | 512 MB         |
| 0011     | Q2 2014   | Computer Module Revision 1.0 512MB, (Sony)                 | 512 MB         |
| 0012     | Q4 2014   | Model A+ Revision 1.0 256MB, (Sony)                        | 256 MB         |
| 0013     | Q1 2015   | Model B+ Revision 1.2 512MB, (Samsung)                     | 512 MB         |
| 0014     |           |                                                            | 512 MB         |
| 0015     |           |                                                            | 256 MB / 512 MB |
| a01041   | Q1 2015   | RsPi 2 Model B Revision 1.1 1GB, (Sony)                    | 1 GB           |
| a21041   | Q1 2015   | RsPi 2 Model B Revision 1.1 1GB, (Embest, China)           | 1 GB           |
| a22042   | Q3 2016   | 2 Model B (Broadcom BCM2837)                               | 1 GB           |
| 900032   | 2014      | B+ Revision 1.2 (Sony UK)                                  | 512 MB         |
| 900092   | Q4 2015   | Zero                                                       | 512 MB         |
| 900093   | Q2 2016   | Zero                                                       | 512 MB         |
| 920093   | Q4 2016?  | Zero                                                       | 512 MB         |
| 9000c1   |           | Zero W                                                     | 512 MB         |
| a02082   | Q1 2016   | 3 Model B                                                  | 1 GB           |
| a22082   | Q1 2016   | 3 Model B                                                  | 1 GB           |
| a020d3   | 2017      | 3 Model B+                                                 | 1 GB           |
| 9020e0   | 2018      | 3 Model A+                                                 | 512 MB         |
| a03111   | 2019      | 4 Model B                                                  | 1 GB           |
| b03111   | 2019      | 4 Model B                                                  | 2 GB           |
| c03111   | 2019      | 4 Model B                                                  | 4 GB           |
| d03114   | 2019      | 4 Model B                                                  | 8 GB           |
| c03130   | Q4 2020   | 4 Model 400                                                | 4 GB           |

*Source : <a href="https://fr.wikipedia.org/wiki/Raspberry_Pi" target="_blank">Wikipedia</a>*

```

On peut récupérer les informations sur le **processeur** et la **version du Raspberry Pi** grâce à la commande suivante :
```bash
cat /proc/cpuinfo
```
Voici la **sortie** de la commande pour le **Raspberry Pi 4** :
```bash
bayart@raspberrypi:~ $ cat /proc/cpuinfo
processor	: 0
BogoMIPS	: 108.00
Features	: fp asimd evtstrm crc32 cpuid
CPU implementer	: 0x41
CPU architecture: 8
CPU variant	: 0x0
CPU part	: 0xd08
CPU revision	: 3

processor	: 1
BogoMIPS	: 108.00
Features	: fp asimd evtstrm crc32 cpuid
CPU implementer	: 0x41
CPU architecture: 8
CPU variant	: 0x0
CPU part	: 0xd08
CPU revision	: 3

processor	: 2
BogoMIPS	: 108.00
Features	: fp asimd evtstrm crc32 cpuid
CPU implementer	: 0x41
CPU architecture: 8
CPU variant	: 0x0
CPU part	: 0xd08
CPU revision	: 3

processor	: 3
BogoMIPS	: 108.00
Features	: fp asimd evtstrm crc32 cpuid
CPU implementer	: 0x41
CPU architecture: 8
CPU variant	: 0x0
CPU part	: 0xd08
CPU revision	: 3

Revision	: d03114
Serial		: 100000003d8b02fb
Model		: Raspberry Pi 4 Model B Rev 1.4
```
La commande `pinout` permet de récupérer le **plan des GPIO** en plus d'informations sur le Raspberry Pi :

```bash
bayart@raspberrypi:~ $ pinout
Description        : Raspberry Pi 4B rev 1.4
Revision           : d03114
SoC                : BCM2711
RAM                : 8GB
Storage            : MicroSD
USB ports          : 4 (of which 2 USB3)
Ethernet ports     : 1 (1000Mbps max. speed)
Wi-fi              : True
Bluetooth          : True
Camera ports (CSI) : 1
Display ports (DSI): 1

,--------------------------------.
| oooooooooooooooooooo J8   +======
| 1ooooooooooooooooooo  J14 |   Net
|  Wi                    12 +======
|  Fi  Pi Model 4B  V1.4 oo      |
| |D     ,---. +---+          +====
| |S     |SoC| |RAM|          |USB3
| |I     `---' +---+          +====
| |0                C|           |
| oo1 J2            S|        +====
|                   I| |A|    |USB2
| pwr   |hd|   |hd| 0| |u|    +====
`-| |---|m0|---|m1|----|x|-------'

J8:
   3V3  (1) (2)  5V    
 GPIO2  (3) (4)  5V    
 GPIO3  (5) (6)  GND   
 GPIO4  (7) (8)  GPIO14
   GND  (9) (10) GPIO15
GPIO17 (11) (12) GPIO18
GPIO27 (13) (14) GND   
GPIO22 (15) (16) GPIO23
   3V3 (17) (18) GPIO24
GPIO10 (19) (20) GND   
 GPIO9 (21) (22) GPIO25
GPIO11 (23) (24) GPIO8 
   GND (25) (26) GPIO7 
 GPIO0 (27) (28) GPIO1 
 GPIO5 (29) (30) GND   
 GPIO6 (31) (32) GPIO12
GPIO13 (33) (34) GND   
GPIO19 (35) (36) GPIO16
GPIO26 (37) (38) GPIO20
   GND (39) (40) GPIO21

J2:
GLOBAL ENABLE (1)
          GND (2)
          RUN (3)

J14:
TR01 TAP (1) (2) TR00 TAP
TR03 TAP (3) (4) TR02 TAP

For further information, please refer to https://pinout.xyz/
```