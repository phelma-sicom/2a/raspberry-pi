# 🐍 Bibliothèques de code et logiciels

## En Python

### OpenCV
[OpenCV](https://opencv.org/) (Open Source Computer Vision Library) est une bibliothèque de **vision par ordinateur et de traitement d'images en open source**. Elle est largement utilisée pour des applications telles que la détection de visage, la reconnaissance d'objets, le traitement d'images... OpenCV fournit des outils et des algorithmes performants pour travailler sur des **projets de vision par ordinateur**.

Pour l'installation avec pip :

```bash
pip install opencv-python
```

### Picamera2
[Picamera2](https://github.com/raspberrypi/picamera2) est une bibliothèque Python conçue pour **interagir avec les caméras du Raspberry Pi**. Elle est le successeur de la bibliothèque Picamera et offre une **interface plus moderne et plus puissante** pour capturer des images et des vidéos à partir des modules caméra compatibles avec Raspberry Pi. Picamera2 **simplifie la configuration et le contrôle** des caméras en Python.

Pour l'installation avec pip :

```bash
pip install picamera2
```

### pySerial
La bibliothèque [pySerial](https://pyserial.readthedocs.io/en/latest/pyserial.html) facilite la communication entre Python et les **ports série**. Elle est conçue pour encapsuler l'accès aux ports série et fournir une manière **simple et efficace** de manipuler ces connexions dans des applications Python. Elle peut être utilisée pour effectuer la communication entre le Raspberry Pi et l'Arduino.

Pour l'installation avec pip :

```bash
pip install pyserial
```

### gpiozero
La bibliothèque [gpiozero](https://gpiozero.readthedocs.io/en/latest/) est conçue pour **contrôler les GPIO (General Purpose Input/Output)** du Raspberry Pi de manière **simple et intuitive**. Elle est **idéale pour débuter** et offre un moyen facile de créer des projets interactifs.

Pour l'installation avec pip :

```bash
pip install gpiozero
```

### pyFirmata
La bibliothèque [pyFirmata](https://github.com/tino/pyFirmata) est un [wrapper](https://fr.wikipedia.org/wiki/Fonction_wrapper) Python pour la bibliothèque [Firmata](https://github.com/firmata/arduino), permettant à des **scripts Python** de communiquer avec un **Arduino**. Elle facilite l'envoi de **commandes Arduino** depuis un ordinateur, permettant ainsi de contrôler des entrées et des sorties numériques et analogiques directement depuis un **script Python**.

Pour l'installation avec pip :

```bash
pip install pyFirmata
```

## Logiciels et commandes bash

### ssh
`ssh` (Secure Shell) est un **protocole** et un **programme** permettant de **se connecter de manière sécurisée** au Raspberry Pi sur un réseau. Il chiffre les données échangées pour protéger la confidentialité et l'intégrité des informations. SSH est couramment utilisé pour accéder à des serveurs distants, exécuter des commandes, transférer des fichiers et pour des tunnels sécurisés.

### scp
`scp` (Secure Copy) est une **commande basée sur SSH** pour **copier des fichiers et des répertoires** entre des hôtes distants de manière sécurisée. Elle utilise le **même mécanisme de chiffrement que SSH** pour protéger les données pendant le transfert. Il offre ainsi une méthode sûre pour **déplacer des fichiers entre différents systèmes**.

### rpicam
`rpicam` est un utilitaire de ligne de commande pour **contrôler la caméra** du Raspberry Pi. Il permet de **capturer des images et des vidéos**, de configurer les paramètres de la caméra, et de diffuser en continu les flux vidéo. Cet outil est utile pour les projets de surveillance, de capture vidéo, et de vision par ordinateur sur les appareils Raspberry Pi équipés d'une caméra.

### Lynx
`Lynx` est un **navigateur web en mode texte** qui permet de naviguer sur le web à partir d'une interface en ligne de commande. Il est particulièrement utile sur les systèmes sans interface graphique ou lorsque l'accès à Internet doit être effectué à partir d'un environnement de terminal comme sur le Raspberry Pi. Lynx est apprécié pour sa légèreté et sa rapidité.


