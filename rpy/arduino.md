# 🔌 Connexion à l'Arduino

La manière la plus simple de connecter l'Arduino au Raspberry Pi est d'utiliser une [transmission série](https://fr.wikipedia.org/wiki/Transmission_série) en [USB](https://fr.wikipedia.org/wiki/USB) (il est aussi possible de faire la connexion avec les [ports GPIO](https://fr.wikipedia.org/wiki/General_Purpose_Input/Output) du Raspberry Pi et de l'Arduino).

## Création

Le code suivant pour l'Arduino initialise la communication série à 9600 bauds et envoie un message toutes les secondes :

```c
void setup() {
  Serial.begin(9600);
}

void loop() {
  Serial.println("Premier message depuis l'Arduino !");
  delay(1000);
}
```

## Vérification de la connexion entre le Raspberry Pi et l'Arduino

Avec la commande `lsusb`, vous pouvez vérifier que l'Arduino est **bien détecté** par le Raspberry Pi :

```bash
bayart@raspberrypi:~ $ lsusb
Bus 002 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub
Bus 001 Device 003: ID 2341:0043 Arduino SA Uno R3 (CDC ACM)
Bus 001 Device 002: ID 2109:3431 VIA Labs, Inc. Hub
Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub
```

## Lecture des données série sur le Raspberry Pi

Vous pouvez utiliser le script Python suivant pour lire les messages envoyés par l'Arduino :

```python
import serial

# Initialiser la connexion série avec l'Arduino
ser = serial.Serial('/dev/ttyACM0', 9600, timeout=1)
ser.reset_input_buffer()

# Boucle pour lire les messages envoyés par l'Arduino
while True:
    if ser.in_waiting > 0:
        line = ser.readline().decode('utf-8').rstrip()
        print(line)
```

### Explications :

1. **Initialisation de la connexion série** :
    ```python
    ser = serial.Serial('/dev/ttyACM0', 9600, timeout=1)
    ser.reset_input_buffer()
    ```
    - `'/dev/ttyACM0'` : C'est le port série sur lequel l'Arduino est connecté. Il peut varier selon les configurations (par exemple, `/dev/ttyUSB0`).
    - `9600` : La vitesse de communication en bauds, qui doit correspondre à celle définie dans le code Arduino.
    - `timeout=1` : Définit le délai d'attente pour les opérations de lecture.

2. **Lecture des données** :
    ```python
    while True:
        if ser.in_waiting > 0:
            line = ser.readline().decode('utf-8').rstrip()
            print(line)
    ```
    - `ser.in_waiting` : Vérifie si des données sont disponibles pour la lecture.
    - `ser.readline()` : Lit une ligne de données terminée par un caractère de nouvelle ligne (`\n`).
    - `decode('utf-8').rstrip()` : Décode les bytes en string et supprime les espaces en fin de chaîne.

Avec cette configuration, on peut établir une communication série fiable entre le Raspberry Pi et l'Arduino.