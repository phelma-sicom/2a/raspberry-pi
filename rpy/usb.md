# 🔌 En USB Gadget

Schéma de la connexion du Raspberry Pi à distance en SSH :

<a href="./_static/usb.png" target="_blank"><img src="./_static/usb.png" alt="Connexion en ssh usb" /></a>

Pour **plus d'informations** sur le protocole SSH : {cite}`alma991004288329706161`

## Étapes pour se connecter à distance au Raspberry Pi

1. **Préparer le système d'exploitation du Raspberry Pi**

- *Pour une connexion USB Gadget* :

  Il faut définir un login et un mot de passe qui seront utilisés pour se connecter en SSH. De plus, il faut configurer le Raspberry Pi pour qu'il fonctionne en mode USB Gadget, ce qui permet d'utiliser un seul câble USB pour l'alimentation et la connexion réseau.

2. **Configurer le Raspberry Pi pour l'USB Gadget**

   - Éditer le fichier `/boot/config.txt` et ajouter la ligne suivante :
     ```bash
     dtoverlay=dwc2
     ```
   - Éditer le fichier `/boot/cmdline.txt` et ajouter `modules-load=dwc2,g_ether` après `rootwait`.
   - Créer un fichier vide nommé `ssh` dans la partition `/boot` pour activer SSH.

3. **Configurer les adresses IP statiques**

   - Installer `dhcpcd` sur le Raspberry Pi avec la commande suivante :
     ```bash
     sudo apt-get install dhcpcd5
     ``` 

   - Éditer le fichier `/etc/dhcpcd.conf` sur le Raspberry Pi et ajouter les lignes suivantes pour configurer une adresse IP statique pour l'interface USB :
     ```bash
     interface usb0
     static ip_address=192.168.7.2/24
     static routers=192.168.7.1
     static domain_name_servers=192.168.7.1
     ```
   - Sur l'ordinateur hôte, configurer une adresse IP statique pour l'interface réseau USB. Par exemple, sous Linux, utiliser la commande suivante :
     ```bash
     sudo ip addr add 192.168.7.1/24 dev usb0
     ```

4. **Connecter le Raspberry Pi à l'ordinateur via un câble USB**

   Utiliser un câble USB pour connecter le Raspberry Pi à votre ordinateur. Cela fournira à la fois l'alimentation et la connexion réseau.

5. **Allumer le Raspberry Pi**

   Le démarrage du Raspberry Pi s'effectue automatiquement lorsqu'il est connecté à l'ordinateur via le câble USB-C (avec un câble permettant de faire passer l'alimentation et le transfert des données).

6. **Se connecter en SSH au Raspberry Pi**

   Utiliser la commande suivante pour se connecter en SSH, en remplaçant `{user}` par votre nom d'utilisateur :
   ```bash
   ssh {user}@192.168.7.2
   ```

``````{important}
À la **première connexion** au Raspberry Pi en **SSH**, une **clé d'hôte** est enregistrée à cet emplacement `/Users/{votre identifiant}/.ssh/known_hosts`. Si vous **changez votre support du système d'exploitation** (carte SD ou SSD), vous **devez supprimer la ligne** correspondant à l'adresse IP du Raspberry Pi dans le fichier `/Users/{votre identifiant}/.ssh/known_hosts`. Sinon, vous obtiendrez ce **message d'erreur** :

```bash
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
@    WARNING: REMOTE HOST IDENTIFICATION HAS CHANGED!     @
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
IT IS POSSIBLE THAT SOMEONE IS DOING SOMETHING NASTY!
Someone could be eavesdropping on you right now (man-in-the-middle attack)!
It is also possible that a host key has just been changed.
The fingerprint for the ECDSA key sent by the remote host is
SHA256:ynbT1HghchCNWT/rhimrePo2SNKVXMN7Ntx55ZBLzkw.
Please contact your system administrator.
Add correct host key in /Users/{votre identifiant}/.ssh/known_hosts to get rid of this message.
Offending ECDSA key in /Users/{votre identifiant}/.ssh/known_hosts:4
ECDSA host key for 192.168.88.63 has changed and you have requested strict checking.
Host key verification failed.
```
``````

6. **Passer les commandes au Raspberry Pi**