# 📺 Connexion à distance en interface graphique

*Pré-requis :*

- Savoir **se connecter** au Raspberry Pi en SSH
- Connaître l'**adresse IP** du Raspberry Pi. Il s'agit de la même adresse que pour la connexion SSH. Vous pouvez récupérer l'information en tapant la commande :

```bash
hostname -I
```

- Avoir un **login** et un **mot de passe** pour se connecter en VNC (même login et mot de passe que pour le SSH)

1. **Activer VNC sur le Raspberry Pi**

Connectez-vous en SSH sur le Raspberry Pi puis tapez la commande suivante pour accéder au menu de configuration :

```bash
sudo raspi-config
```

Allez dans le menu "Options d'interface" et activez l'option VNC.

2. **Installer un client VNC sur l'ordinateur où doit être visualisée l'interface graphique du Raspberry Pi**

Il existe différents **clients VNC**, vous pouvez installer RealVNC Viewer : https://www.realvnc.com/fr/connect/download/viewer/

3. **Se connecter au serveur VNC du Raspberry Pi**