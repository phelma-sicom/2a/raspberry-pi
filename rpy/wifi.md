# 📶 En WiFi

Schéma de la connexion du Raspberry Pi à distance en SSH :

<a href="./_static/ssh.png" target="_blank"><img src="./_static/ssh.png" alt="Connexion en ssh" /></a>

Pour **plus d'informations** sur le protocole SSH : {cite}`alma991004288329706161`

## Étapes pour se connecter à distance au Raspberry Pi

1. **Préparer le système d'exploitation du Raspberry Pi**

- *Pour une connexion WiFi* :

  Il faut définir un login et un mot de passe qui seront utilisés pour se connecter en SSH. Il est également nécessaire d'activer la connexion WiFi et de configurer le nom du réseau WiFi ainsi que le mot de passe pour y accéder.

2. **Connecter le disque de données au Raspberry Pi**

3. **Allumer le Raspberry Pi**

   Le démarrage du Raspberry Pi s'effectue en branchant l'alimentation électrique.

4. **Récupérer l'adresse IP du Raspberry Pi sur le réseau**

   Très souvent, sur le réseau, l'adresse IP locale du Raspberry Pi est fournie de manière automatique par un <a href="https://fr.wikipedia.org/wiki/Dynamic_Host_Configuration_Protocol" target="_blank">serveur DHCP</a>. Il faut trouver un moyen de récupérer cette adresse :

   - Si vous utilisez votre smartphone comme routeur WiFi, c'est votre smartphone qui est le serveur DHCP. Vous pouvez donc trouver l'adresse IP du Raspberry Pi dans les paramètres du routeur.

5. **Se connecter en SSH au Raspberry Pi**

``````{important}
À la **première connexion** au Raspberry Pi en **SSH**, une **clé d'hôte** est enregistrée à cet emplacement `/Users/{votre identifiant}/.ssh/known_hosts`. Si vous **changez votre support du système d'exploitation** (carte SD ou SSD), vous **devez supprimer la ligne** correspondant à l'adresse IP du Raspberry Pi dans le fichier `/Users/{votre identifiant}/.ssh/known_hosts`. Sinon, vous obtiendrez ce **message d'erreur** :
```bash
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
@    WARNING: REMOTE HOST IDENTIFICATION HAS CHANGED!     @
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
IT IS POSSIBLE THAT SOMEONE IS DOING SOMETHING NASTY!
Someone could be eavesdropping on you right now (man-in-the-middle attack)!
It is also possible that a host key has just been changed.
The fingerprint for the ECDSA key sent by the remote host is
SHA256:ynbT1HghchCNWT/rhimrePo2SNKVXMN7Ntx55ZBLzkw.
Please contact your system administrator.
Add correct host key in /Users/{votre identifiant}/.ssh/known_hosts to get rid of this message.
Offending ECDSA key in /Users/{votre identifiant}/.ssh/known_hosts:4
ECDSA host key for 192.168.88.63 has changed and you have requested strict checking.
Host key verification failed.
