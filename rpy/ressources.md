<style>
    /* Pour la gestion des iframes */
 .container {
  position: relative;
  overflow: hidden;
  width: 100%;
  padding-top: 56.25%; /* 16:9 Aspect Ratio (divide 9 by 16 = 0.5625) */
}
.responsive-iframe {
  position: absolute;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  width: 100%;
  height: 100%;
}
</style>

# 🚀 Ressources complémentaires

Tous les informations suivantes sont spécifiques pour le **Raspberry Pi 4 8Go** utilisé à Phelma. Pour plus d'informations sur d'**autres versions du Raspberry Pi**, vous pouvez aller sur le **site officiel** : https://www.raspberrypi.com/

Pour des **tutoriels de montages** avec des Raspberry Pi, vous pouvez aller sur cette ressource : https://learn.adafruit.com

Pour des **livres** sur le Raspberry Pi, voici quelques références :

<a href="https://beluga.univ-grenoble-alpes.fr/permalink/33UGRENOBLE_INST/1vb34gl/alma991007498492606161" target="_blank"><img src="https://proxy-euf.hosted.exlibrisgroup.com/exl_rewrite/syndetics.com/index.php?client=primo&isbn=1-119-79687-3/sc.jpg" alt="Raspberry Pi for dummies" height="100px"/></a> 

<a href="https://beluga.univ-grenoble-alpes.fr/permalink/33UGRENOBLE_INST/1vb34gl/alma991007498492606161" target="_blank">Raspberry Pi for dummies</a>, McManus, Sean and Cook, Mike, 2021 {cite}`alma991007498492606161`

<a href="https://beluga.univ-grenoble-alpes.fr/permalink/33UGRENOBLE_INST/1vb34gl/alma991006923232606161" target="_blank"><img src="https://bu.univ-grenoble-alpes.fr/FormAlmaElectre/electre2022b.php?ean=9782409032776" alt="Raspberry Pi 4 : exploitez tout le potentiel de votre nano-ordinateur (inclus : un projet de station météo)" height="100px"/></a> 

<a href="https://beluga.univ-grenoble-alpes.fr/permalink/33UGRENOBLE_INST/1vb34gl/alma991006923232606161" target="_blank">Raspberry Pi 4 : exploitez tout le potentiel de votre nano-ordinateur (inclus : un projet de station météo)</a>,Mocq François , 2021 {cite}`alma991006923232606161`

<a href="https://beluga.univ-grenoble-alpes.fr/permalink/33UGRENOBLE_INST/1vb34gl/alma991007447025606161" target="_blank"><img src="https://proxy-euf.hosted.exlibrisgroup.com/exl_rewrite/syndetics.com/index.php?client=primo&isbn=3-03842-579-6/sc.jpg" alt="Raspberry Pi technology" height="100px"/></a> 

<a href="https://beluga.univ-grenoble-alpes.fr/permalink/33UGRENOBLE_INST/1vb34gl/alma991007447025606161" target="_blank">Raspberry Pi technology</a>, Cox, Simon J. and Johnston, Steven J., 2017 {cite}`alma991007447025606161`

___

## 📝 Papiers de recherche utilisant le Raspberry Pi

- Tri des noix par **reconnaissance d'images** sur une ligne de **production de cerneaux de noix** {cite}`WangYingbiao2024AolY`

- Détection du **stress hydrique** par reconnaissances d'images dans des champs {cite}`ChandelNarendraSingh2024SAmd`

- Exemples de **cas d'usages** du Raspberry Pi (premières versions A et B) en **science** et dans l'**enseignement** {cite}`hal-02629562`

___

## 📚 Fiche technique Raspberry Pi 4

<a href="https://datasheets.raspberrypi.com/rpi4/raspberry-pi-4-datasheet.pdf" target="_blank">Téléchargement</a>

<div class="container">
  <iframe class="responsive-iframe" src="https://datasheets.raspberrypi.com/rpi4/raspberry-pi-4-datasheet.pdf" scrolling="no" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen ></iframe>
</div>

___

## 📚 Résumé fiche technique Raspberry Pi 4

<a href="https://datasheets.raspberrypi.com/rpi4/raspberry-pi-4-product-brief.pdf" target="_blank">Téléchargement</a>

<div class="container">
  <iframe class="responsive-iframe" src="https://datasheets.raspberrypi.com/rpi4/raspberry-pi-4-product-brief.pdf" scrolling="no" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen ></iframe>
</div>

___

## 📐 Schéma mécanique avec cotes du Raspberry Pi 4

<a href="https://datasheets.raspberrypi.com/rpi4/raspberry-pi-4-mechanical-drawing.pdf" target="_blank">Téléchargement</a>

<div class="container">
  <iframe class="responsive-iframe" src="https://datasheets.raspberrypi.com/rpi4/raspberry-pi-4-mechanical-drawing.pdf" scrolling="no" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen ></iframe>
</div>

___

## 📐 Schéma mécanique avec cotes de la caméra module V2

<a href="https://datasheets.raspberrypi.com/camera/camera-module-2-mechanical-drawing.pdf" target="_blank">Téléchargement</a>

<div class="container">
  <iframe class="responsive-iframe" src="https://datasheets.raspberrypi.com/camera/camera-module-2-mechanical-drawing.pdf" scrolling="no" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen ></iframe>
</div> 

___

## 📚 Documentation technique de la bibliothèque Python Picamera2

<a href="https://datasheets.raspberrypi.com/camera/picamera2-manual.pdf" target="_blank">Téléchargement</a>

<div class="container">
  <iframe class="responsive-iframe" src="https://datasheets.raspberrypi.com/camera/picamera2-manual.pdf" scrolling="no" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen ></iframe>
</div> 
