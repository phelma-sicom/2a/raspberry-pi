<img src='./rpy/logo.png' height='100'/> 

# Plateforme pour l'utilisation du Raspberry Pi
<img src='https://mirrors.creativecommons.org/presskit/buttons/88x31/svg/by-nc.svg' height='31'/>

___

<img src='./rpy/_static/rpy.png' height='300'/> 

🌐 Lien vers la **plateforme** : https://phelma-sicom.gricad-pages.univ-grenoble-alpes.fr/2a/raspberry-pi/

___

Cette plateforme contient les **informations** suivantes sur le Raspberry Pi :

- Schémas de la carte
- Différentes manières de s'y connecter
- Utilisation de la caméra
- Bibliothèques de code utiles
- Exemples de cas d'usage
- ...
